#ifndef LEVEL_H_
#define LEVEL_H_
#include <stdint.h>

extern uint8_t map_x, map_y;
extern uint8_t reload_map;

extern uint8_t collision_map[120];
extern uint8_t death_map[120];


extern void load_map(const uint8_t *);

#endif
