#define PPU_CTRL    (uint8_t *)0x2000
#define PPU_MASK    (uint8_t *)0x2001
#define PPU_STATUS  (uint8_t *)0x2002
#define OAM_ADDRESS (uint8_t *)0x2003
#define OAM_DATA    (uint8_t *)0x2004
#define PPU_SCROLL  (uint8_t *)0x2005
#define PPU_ADDRESS (uint8_t *)0x2006
#define PPU_DATA    (uint8_t *)0x2007

#define SPRITE_DATA (uint8_t *)0x0200

#define PPU_PATTERN_TABLE_0_ADDRESS 0x0000
#define PPU_PATTERN_TABLE_1_ADDRESS 0x1000

#define PPU_NAMETABLE_0_ADDRESS 0x2000
#define PPU_NAMETABLE_1_ADDRESS 0x2400
#define PPU_NAMETABLE_2_ADDRESS 0x2800
#define PPU_NAMETABLE_3_ADDRESS 0x2C00

#define PPU_PALETTE_ADDRESS 0x3F00

#define SET_PALETTE(offset, a, b, c, d) { \
    *PPU_ADDRESS = 0x3F;                  \
    *PPU_ADDRESS = offset;                \
    *PPU_DATA = a;                        \
    *PPU_DATA = b;                        \
    *PPU_DATA = c;                        \
    *PPU_DATA = d;                        \
}

#define SET_PPU_ADDRESS(address) { \
    *PPU_ADDRESS = address >> 8;   \
    *PPU_ADDRESS = address;        \
}
