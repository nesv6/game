#include <stdint.h>
#include "map_data.h"
#include "map.h"
#include "controller.h"
#include "stars.h"
#include "player.h"
#include "ppu.h"

uint8_t nmi_occurred;

uint8_t map_x = 7, map_y = 3;
uint8_t reload_map = 0;

uint8_t count;

uint8_t collision_map[120];
uint8_t death_map[120];


extern void wait_vblank(void);


void initialize(void)
{
    // Load the palettes
    SET_PALETTE(0x10, 0x0F, 0x21, 0x30, 0x00);
    SET_PALETTE(0x14, 0x0F, 0x00, 0x10, 0x30);
    SET_PPU_ADDRESS(0x0000);

    init_stars();
    init_player();

    *PPU_CTRL = 0x90;
    wait_vblank();

    load_map(MAPS[map_y][map_x]);
}

void loop(void)
{
    count += 1;
    update_player(read_controller());
    update_stars();

    if (reload_map) {
        const uint8_t *map = MAPS[map_y][map_x];
        if (map)
            load_map(map);

        reload_map = 0;
        return;
    }

    wait_vblank();
}

void main(void)
{
    initialize();
    while (1)
        loop();
}
